package com.assuncao.armando.plazamovies.net;

import android.net.Uri;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

public class HttpClient {
    private String address;
    private String method;
    private int statusCode;
    private String response;
    private List<List<String>> parameters; //use with method POST
    private HttpURLConnection connection;

    public static final String GET = "GET";
    public static final String POST = "POST";
    public static final String PUT = "PUT";
    public static final String DELETE = "DELETE";
    public static final String HEAD = "HEAD";
    public static final String OPTIONS = "OPTIONS";
    private static List<HttpCookie> cookies;
    private static final int TIMEOUT_CONECTION = 5000; //5s
    @SuppressWarnings("unused")
    private static final int TIMEOUT_WAIT_DATA = 1000*60*15; //15min
    private final String USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36";
    private boolean acceptGzip;
    private String encoding;

    public HttpClient(String address, String method, String encoding) throws ProtocolException, MalformedURLException, IOException{
        this.address = address;
        this.encoding = encoding;
        acceptGzip = true;
        this.method = method;
        URL obj = new URL(address);
        this.connection = (HttpURLConnection) obj.openConnection();
        connection.setRequestProperty("Content-Type", "text/html; charset=UTF-8");
        switch (method.toUpperCase()){
            case HttpClient.POST:{
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                break;
            }
            case HttpClient.GET:{
                connection.setRequestMethod("GET");
                break;
            }
            case HttpClient.PUT:{
                connection.setRequestMethod("PUT");
                break;
            }
            case HttpClient.DELETE:{
                connection.setRequestMethod("DELETE");
                break;
            }
            case HttpClient.HEAD:{
                connection.setRequestMethod("HEAD");
                break;
            }
            case HttpClient.OPTIONS:{
                connection.setRequestMethod("OPTIONS");
                break;
            }
            default:{
                throw new IllegalArgumentException("Tipo inválido");
            }
        }
        connection.setRequestProperty("User-Agent", USER_AGENT);
        connection.setRequestProperty("Accept-Language", "pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4");
        connection.setRequestProperty("Accept-Encoding", "gzip, deflate");
        connection.setRequestProperty("Accept", "*/*");
        this.parameters = new ArrayList<List<String>>();
    }

    public HttpClient(String address, String method) throws ProtocolException, MalformedURLException, IOException {
        this(address, method, null);
    }

    public void addParameter(String field, String value) {
        List<String> pair = new ArrayList<String>();
        pair.add(field);
        pair.add(value);
        this.parameters.add(pair);
    }

    public void clearParameters() {
        parameters.clear();
        parameters = null;
    }

    public int getStatus() {
        return statusCode;
    }

    public String getTextoResposta(){
        return this.response;
    }

    public InputStream getContent() throws IOException{
        return connection.getInputStream();
    }

    public synchronized static void clearCookies(){
        cookies.clear();
        cookies = null;
    }

    public synchronized static List<HttpCookie> getCookies(){
        return cookies;
    }

    public void execute() throws  IOException{
        Uri.Builder builder;
        builder = new Uri.Builder();
        String urlParameters = "";

        // Send post request
        if(method.equalsIgnoreCase(HttpClient.POST)) {
            if(parameters.size() > 0) {
                for (List<String> pair : parameters) {
                    builder.appendQueryParameter(pair.get(0), pair.get(1));
                }
                urlParameters = builder.build().getEncodedQuery();
            }

            connection.setDoInput(true);
            connection.setDoOutput(true);
            //DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            //wr.writeBytes(urlParameters);

            OutputStream os = connection.getOutputStream();
            BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

            wr.write(urlParameters);
            wr.flush();
            wr.close();
        }

        this.statusCode = connection.getResponseCode();
        System.out.println(String.format("\nSending '%s' request to URL : %s", method, address));
        System.out.println("Parameters : " + urlParameters);
        System.out.println("Response Code : " + statusCode);

        InputStream inputStream = null;
        boolean isGzip = false;
        if(connection.getContentEncoding() != null){
            isGzip = connection.getContentEncoding().equalsIgnoreCase("gzip");
        }
        if (isGzip){
            inputStream = new GZIPInputStream(connection.getInputStream());
        }
        else{
            inputStream = connection.getInputStream();
        }

        BufferedReader in = null;
        if(encoding != null) {
            in = new BufferedReader(new InputStreamReader(inputStream, encoding));
        }
        else{
            in = new BufferedReader(new InputStreamReader(inputStream));
        }

        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        CookieManager cookieManager = new CookieManager();
        CookieHandler.setDefault(cookieManager);

        this.response = response.toString();

        //print result
        System.out.println("response: " + this.response);
    }
}
