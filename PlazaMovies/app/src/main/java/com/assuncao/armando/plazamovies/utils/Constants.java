package com.assuncao.armando.plazamovies.utils;


public final class Constants {
    public static final String SITE_URL = "http://www.cineplaza.com.br";

    public static final String URL_WEBSERVICE = "http://10.0.2.2:32500/plaza_filmes/api/v1";

    private Constants(){};
}