package com.assuncao.armando.plazamovies;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.assuncao.armando.plazamovies.model.NowPlaying;
import com.assuncao.armando.plazamovies.net.task.TaskContent;

public class MainActivity extends AppCompatActivity implements TaskContent.ITaskContent {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TaskContent taskContent = new TaskContent(this);
        taskContent.execute();
    }

    @Override
    public void nowPlaying(NowPlaying nowPlaying) {
        MovieArrayAdapter movieArrayAdapter = new MovieArrayAdapter(this, nowPlaying.getMovies());
        ListView lvMovies = (ListView) findViewById(R.id.listView_movies);
        lvMovies.setAdapter(movieArrayAdapter);
    }

    @Override
    public void error(String msg) {

    }
}
