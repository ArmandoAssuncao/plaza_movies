package com.assuncao.armando.plazamovies.model;

import com.assuncao.armando.plazamovies.utils.Constants;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Movie implements Serializable{
    @SerializedName("url_cover")
    private String cover;
    private String name;
    private String genre;
    private String duration;
    private String rating;
    private String trailer;

    public Movie() {}

    public Movie(String cover, String name, String genre, String duration, String rating, String trailer) {
        setUrlCover(cover);
        this.name = name;
        this.genre = genre;
        this.duration = duration;
        this.rating = rating;
        this.trailer = trailer;
    }

    public String getUrlCover() {
        return Constants.SITE_URL + "/" + cover;
    }
    public void setUrlCover(String cover) {this.cover = cover;}

    public String getName() {return name;}
    public void setName(String name) {this.name = name;}

    public String getGenre() {return genre;}
    public void setGenre(String genre) {this.genre = genre;}

    public String getDuration() {return duration;}
    public void setDuration(String duration) {this.duration = duration;}

    public String getRating() {return rating;}
    public void setRating(String rating) {this.rating = rating;}

    public String getTrailer() {
        return trailer;
    }
    public void setTrailer(String trailer) { this.trailer = trailer;}
}
