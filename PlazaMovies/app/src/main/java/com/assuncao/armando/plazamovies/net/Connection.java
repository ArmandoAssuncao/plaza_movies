package com.assuncao.armando.plazamovies.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public final class Connection {
    public static int TYPE_CONNECTION_NONE = 0;
    public static int TYPE_CONNECTION_MOBILE = 1;
    public static int TYPE_CONNECTION_WIFI = 2;


    private Connection(){}

    public static boolean isConnect(Context contexto) {
        ConnectivityManager cn = (ConnectivityManager)contexto.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo infoRede = cn.getActiveNetworkInfo();

        if(infoRede != null && infoRede.isConnected()){
            return true;
        }
        else{
            return false;
        }
    }

    public static int getTypeConnection(Context context){
        ConnectivityManager conMan = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        int typeConnection;

        NetworkInfo activityNetwork = conMan.getActiveNetworkInfo();

        if(activityNetwork != null) {
            if (activityNetwork.getType() == ConnectivityManager.TYPE_WIFI) //mobile
                typeConnection = TYPE_CONNECTION_MOBILE;
            else if (activityNetwork.getType() == ConnectivityManager.TYPE_MOBILE) //wifi
                typeConnection = TYPE_CONNECTION_WIFI;
            else
                typeConnection = TYPE_CONNECTION_NONE; //unknown network
        }
        else{
            typeConnection = TYPE_CONNECTION_NONE;
        }
        return typeConnection;
    }
}