package com.assuncao.armando.plazamovies;

import android.content.Context;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.assuncao.armando.plazamovies.model.Movie;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

public class MovieArrayAdapter extends ArrayAdapter<Movie> {
    private Context context;

    public MovieArrayAdapter(Context context, ArrayList<Movie> users) {
        super(context, 0, users);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Movie movie = getItem(position);

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_movie, parent, false);
        }

        ImageView ivCover = (ImageView) convertView.findViewById(R.id.item_movie_image_cover);
        TextView tvName = (TextView) convertView.findViewById(R.id.item_movie_name);
        TextView tvGenre = (TextView) convertView.findViewById(R.id.item_movie_genre);
        TextView tvDuration = (TextView) convertView.findViewById(R.id.item_movie_duration);
        TextView tvRating = (TextView) convertView.findViewById(R.id.item_movie_rating);
        TextView tvTrailer = (TextView) convertView.findViewById(R.id.item_movie_trailer);

        if(movie.getUrlCover() != null){
            System.out.println("urlimg " + movie.getUrlCover());

            Ion.with(this.context)
                    .load(movie.getUrlCover())
                    .withBitmap()
//					.placeholder(R.drawable.placeholder_image)
//					.error(R.drawable.error_image)
//					.animateLoad(spinAnimation)
//					.animateIn(fadeInAnimation)
                    .intoImageView(ivCover);
        }
        else{
            ivCover.setVisibility(View.INVISIBLE);
        }

        tvName.setText(movie.getName());
        tvGenre.setText(movie.getGenre());
        tvDuration.setText(movie.getDuration());
        tvRating.setText(movie.getRating());
        tvTrailer.setText(movie.getTrailer());

        return convertView;
    }
}
