package com.assuncao.armando.plazamovies.model;

import java.util.ArrayList;

public class NowPlaying {
    private String horary;
    private ArrayList<Movie> movies;

    public NowPlaying() {}

    public String getHorary() {
        return horary;
    }
    public void setHorary(String horary) {
        this.horary = horary;
    }

    public ArrayList<Movie> getMovies() {
        return movies;
    }
    public void setMovies(ArrayList<Movie> movies) {
        this.movies = movies;
    }

    public void addMovie(Movie movie){
        movies.add(movie);
    }
}
