package com.assuncao.armando.plazamovies.net.task;

import android.content.Context;
import android.os.AsyncTask;

import com.assuncao.armando.plazamovies.model.NowPlaying;
import com.assuncao.armando.plazamovies.net.HttpClient;
import com.assuncao.armando.plazamovies.utils.Constants;
import com.google.gson.Gson;

import java.io.IOException;

public class TaskContent extends AsyncTask<Void, Void, NowPlaying>{

    private Context context;
    private ITaskContent iTask;

    public TaskContent(Context context) {
        this.context = context;
        if(context instanceof ITaskContent){
            iTask = (ITaskContent)context;
        }
        else{
            throw new IllegalArgumentException("Context is not an ITaskContent");
        }
    }

    private NowPlaying accessWebservice(Void... params){
        HttpClient httpClient = null;

        try {
            httpClient = new HttpClient(Constants.URL_WEBSERVICE, HttpClient.GET);

            httpClient.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(httpClient == null) return null;

        String content = httpClient.getTextoResposta();
        Gson gson = new Gson();
        NowPlaying nowPlaying = gson.fromJson(content, NowPlaying.class);

        return nowPlaying;
    }

    @Override
    protected NowPlaying doInBackground(Void... params) {
        NowPlaying nowPlaying = null;

        nowPlaying = accessWebservice(params);

        return nowPlaying;
    }

    @Override
    protected void onPostExecute(NowPlaying nowPlaying) {
        super.onPostExecute(nowPlaying);

        iTask.nowPlaying(nowPlaying);
    }

    public interface ITaskContent{
        void nowPlaying(NowPlaying nowPlaying);
        void error(String msg);
    }
}