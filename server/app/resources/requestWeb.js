var constants = require('../resources/constants');
var request = require("request");

module.exports = {
    get: function(url, querys, callback){
        var obj = {};
        obj.error = 1;
        obj.data;

        var options = {
            url: 'http://www.cineplaza.com.br',
            headers: {
                'Host': 'cineplaza.com.br',
                'Content-Type': 'text/html; charset=utf-8',
                //'Accept-Encoding': 'gzip, deflate, sdch',
                'Accept-Language': 'pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.92 Safari/537.36',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                'Cache-Control': 'max-age=0',
                'Connection': 'keep-alive'
            }
        };

        request.get(options, function (error, response, body) {
            if (!error && response.statusCode == 200){
                obj.error = 0;
                obj.body = response.body;
            }
            else
                console.log(error);
                obj.data = error;

            callback(obj);
        });
    }
};