var requestWeb = require('../resources/requestWeb');
var movieClass = require('../models/movie');
var constants = require('../resources/constants');
var cheerio = require('cheerio');

module.exports = function(app){
	controller = {};

	controller.movies = function(req, res){
		var url = constants.url.SITE;
		var querys = '';

		requestWeb.get(url, querys, function(response){
			resObj = {};
			resObj.error = 0;

			if(response.error != 0){
				resObj.error = response.error;
				res.status(401).json(resObj);
			}
			else{
				var movies = parseBody(response.body);
				res.status(200).json(movies);
			}
		});
	};

	return controller;
};


function parseBody(bodyHtml){
	var $ = cheerio.load(bodyHtml);
	String.prototype.formatToNumber = function() {
    	return this.replace(',','.').replace(/[^0-9,.]/g, '');
	};

	var tableMovies = $('table table tr');

	var horary = tableMovies.first().children('td:nth-child(1)').children('p:nth-child(3)').text().replace("\n","");

	var arrayMovies = [];
	tableMovies.each(function(index, elem){
		if(index != 0 && index != tableMovies.length-1){
			var Movie = new movieClass();
			Movie.url_cover = $(this).children('td:nth-child(1)').children('img').attr('src');
			Movie.name = $(this).children('td:nth-child(3)').children('p').children('font').children('strong').children('span').text().replace("- ", "").trim();
			Movie.genre = $(this).children('td:nth-child(3)').children('p:nth-child(2)').text().replace("Genero: ", "");
			Movie.duration = $(this).children('td:nth-child(3)').children('p:nth-child(3)').text().split("\n")[0].replace("Duraçao: ", "");
			Movie.rating = $(this).children('td:nth-child(3)').children('p:nth-child(3)').text().split("\n")[1].replace("Classificaçao: ", "").trim();
			Movie.trailer = $(this).children('td:nth-child(2)').children('object').children('embed').attr('src');

			arrayMovies.push(Movie);
		}
	});

	var data = {
		'horary': horary,
		'movies': arrayMovies
	}

	return data;
};
